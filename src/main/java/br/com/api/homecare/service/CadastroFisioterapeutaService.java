package br.com.api.homecare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Fisioterapeuta;
import br.com.api.homecare.model.Paciente;
import br.com.api.homecare.repository.FisioterapeutaRepository;
import br.com.api.homecare.repository.PacienteRepository;

@Service
public class CadastroFisioterapeutaService {

	@Autowired
	private FisioterapeutaRepository fisioterapeutaRepository;
	
	public Fisioterapeuta salvar(Fisioterapeuta fisioterapeuta) {
		return fisioterapeutaRepository.save(fisioterapeuta);
	}
	
	public void excluir(Long idFIsio) {
		try {
			fisioterapeutaRepository.deleteById(idFIsio);
			
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(
				String.format("Não existe um cadastro de fisioterapeuta com código %d", idFIsio));
		
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
				String.format("Fisioterapeuta de código %d não pode ser removida, pois está em uso", idFIsio));
		}
	}
	
}
