package br.com.api.homecare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Paciente;
import br.com.api.homecare.repository.PacienteRepository;

@Service
public class CadastroPacienteService {

	@Autowired
	private PacienteRepository pacienteRepository;
	
	public Paciente salvar(Paciente paciente) {
		return pacienteRepository.save(paciente);
	}
	
	public void excluir(Long idPaciente) {
		try {
			pacienteRepository.deleteById(idPaciente);
			
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(
				String.format("Não existe um cadastro de paciente com código %d", idPaciente));
		
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
				String.format("Paciente de código %d não pode ser removida, pois está em uso", idPaciente));
		}
	}
	
}
