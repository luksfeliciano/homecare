package br.com.api.homecare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Psicologo;
import br.com.api.homecare.model.Paciente;
import br.com.api.homecare.repository.FisioterapeutaRepository;
import br.com.api.homecare.repository.FonoaudiologoRepository;
import br.com.api.homecare.repository.PacienteRepository;
import br.com.api.homecare.repository.PsicologoRepository;

@Service
public class CadastroPsicologoService {

	@Autowired
	private PsicologoRepository psicologoRepository;
	
	public Psicologo salvar(Psicologo psicologo) {
		return psicologoRepository.save(psicologo);
	}
	
	public void excluir(Long idPsico) {
		try {
			psicologoRepository.deleteById(idPsico);
			
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(
				String.format("Não existe um cadastro de Psicologo com código %d", idPsico));
		
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
				String.format("Psicologo de código %d não pode ser removida, pois está em uso", idPsico));
		}
	}
	
}
