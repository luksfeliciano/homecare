package br.com.api.homecare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Fonoaudiologo;
import br.com.api.homecare.model.Paciente;
import br.com.api.homecare.repository.FisioterapeutaRepository;
import br.com.api.homecare.repository.FonoaudiologoRepository;
import br.com.api.homecare.repository.PacienteRepository;

@Service
public class CadastroFonoaudiologoService {

	@Autowired
	private FonoaudiologoRepository fonoaudiologoRepository;
	
	public Fonoaudiologo salvar(Fonoaudiologo fonoaudiologo) {
		return fonoaudiologoRepository.save(fonoaudiologo);
	}
	
	public void excluir(Long idFono) {
		try {
			fonoaudiologoRepository.deleteById(idFono);
			
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(
				String.format("Não existe um cadastro de fonoaudiologo com código %d", idFono));
		
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
				String.format("Fonoaudiologo de código %d não pode ser removida, pois está em uso", idFono));
		}
	}
	
}
