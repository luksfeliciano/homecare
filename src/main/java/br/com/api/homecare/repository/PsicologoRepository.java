package br.com.api.homecare.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.api.homecare.model.Psicologo;

@Repository
public interface PsicologoRepository extends JpaRepository<Psicologo, Long>{

}
