package br.com.api.homecare.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table
public class Fonoaudiologo {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fono")
	private Long id;
	
	@Column(name = "nome_fonoaudiologo")
	private String nomeFono;
	
	@Column(name = "telefone_cel")
	private String telefoneCel;
	
	@Column(name = "rg_fonoaudiologo")
	private String rgFono;
	
	@Column(name = "cpf_fonoaudiologo")
	private String cpfFono;
	
	@Column(name = "endereco_fonoaudiologo")
	private String enderecoFisio;
	
	@Column(name = "telefone_fixo")
	private String telefoneFixo;
	
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	
	@Column(name = "data_admissao")
	private Date dataAdmissao;
	
	@Column(name = "numero_crefono")
	private Long nrCrefono;
	
	@Column(name = "especializacao")
	private String especializacao;
	
}
