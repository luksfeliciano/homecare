package br.com.api.homecare.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table
public class Fisioterapeuta {
	
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fisio")
	private Long id;
	
	@Column(name = "nome_fisioterapeuta")
	private String nomeFisio;
	
	@Column(name = "telefone_celular")
	private String telefoneCel;
	
	@Column(name = "rg_fisioterapeuta")
	private String rgFisio;
	
	@Column(name = "cpf_fisioterapeuta")
	private String cpfFisio;
	
	@Column(name = "endereco_fisioterapeuta")
	private String enderecoFisio;
	
	@Column(name = "telefone_fixo")
	private String telefoneFixo;
	
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	
	@Column(name = "data_admissao")
	private Date dataAdmissao;
	
	@Column(name = "numero_crefito")
	private Long nrCrefito;
	
	@Column(name = "especializacao")
	private String especializacao;
	
}
