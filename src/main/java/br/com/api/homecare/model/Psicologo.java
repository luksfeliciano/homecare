package br.com.api.homecare.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table
public class Psicologo {
	
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_psico")
	private Long id;
	
	@Column(name = "nome_pisicologo")
	private String nomePsico;
	
	@Column(name = "telefone_celular")
	private String telefoneCel;
	
	@Column(name = "rg_psicologo")
	private String rgPsico;
	
	@Column(name = "cpf_psicologo")
	private String cpfPsico;
	
	@Column(name = "endereco_pisicologo")
	private String enderecoPsico;
	
	@Column(name = "telefone_fixo")
	private String telefoneFixo;
	
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	
	@Column(name = "data_admissao")
	private Date dataAdmissao;
	
	@Column(name = "numero_crp")
	private Long nrCrp;
	
	@Column(name = "especializacao")
	private String especializacao;
	

}
