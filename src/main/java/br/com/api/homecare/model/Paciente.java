package br.com.api.homecare.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table
public class Paciente {
	
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_paciente")
	private Long id;

	@Column(name = "nome_paciente")
	private String nomePaciente;
	
	@Column(name = "rg_paciente")
	private String rgPaciente;
	
	@Column(name = "cpf_paciente")
	private String cpfPaciente;
	
	@Column(name = "telefone")
	private String telefone;
	
	@Column(name = "situacao_paciente")
	private Boolean stPaciente;
	
	@Column(name = "endereco")
	private String endereco;
	
	@Column(name = "nome_cuidador")
	private String nomeCuidador;
	
	@Column(name = "contato_cuidador")
	private String contatoCuidador;
	
	@Column(name = "convenio_pcte")
	private String convenioPcte;
	
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	
	@Column(name = "data_auditoria")
	private Date dataAuditoria;
	
	@Column(name = "diagnostico")
	private String diagnostico;

	@Column(name = "quantidade_atendimento")
	private Long qtdAtendimento;
	
	@Column(name = "numero_matricula_convenio")
	private Long numeMatriculaConvenio;
	
	@Column(name = "sexo")
	private String sexo;
	
	@Column(name = "cep")
	private String cep;
	
	@Column(name = "logradouro")
	private String logradouro;
	
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "bairro")
	private String bairro;
	
	@Column(name = "cidade")
	private String cidade;
	
	@Column(name = "estado")
	private String estado;
	
	@Column(name = "observacao")
	private String observacao;
	
	@ManyToOne
	private Fisioterapeuta fisioterapeuta;
	
	@ManyToOne
	private Psicologo psicologo;
	
	@ManyToOne
	private Fonoaudiologo fonoaudiologo;

}
