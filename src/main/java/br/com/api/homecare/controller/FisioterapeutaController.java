package br.com.api.homecare.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Fisioterapeuta;
import br.com.api.homecare.model.Fisioterapeuta;
import br.com.api.homecare.repository.FisioterapeutaRepository;
import br.com.api.homecare.repository.PacienteRepository;
import br.com.api.homecare.service.CadastroFisioterapeutaService;
import br.com.api.homecare.service.CadastroPacienteService;

@RestController
@RequestMapping(value = "/fisioterapeutas")
public class FisioterapeutaController {

	@Autowired
	private FisioterapeutaRepository fisioterapeutaRepository;
	
	@Autowired
	private CadastroFisioterapeutaService cadastroFisioterapeutaService;
	
	@GetMapping
	public List<Fisioterapeuta> listar() {
		return fisioterapeutaRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Fisioterapeuta> buscar(@PathVariable Long id) {
		Optional<Fisioterapeuta> paciente = fisioterapeutaRepository.findById(id);
		
		if (paciente.isPresent()) {
			return ResponseEntity.ok(paciente.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Fisioterapeuta adicionar(@RequestBody Fisioterapeuta fisioterapeuta) {
		return cadastroFisioterapeutaService.salvar(fisioterapeuta);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Fisioterapeuta> atualizar(@PathVariable Long id,
			@RequestBody Fisioterapeuta fisioterapeuta) {
		Optional<Fisioterapeuta> fisioterapeutaAtual = fisioterapeutaRepository.findById(id);
		
		if (fisioterapeutaAtual.isPresent()) {
			BeanUtils.copyProperties(fisioterapeuta, fisioterapeutaAtual.get(), "id");
			
			Fisioterapeuta fisioterapeutaSalvo = cadastroFisioterapeutaService.salvar(fisioterapeutaAtual.get());
			return ResponseEntity.ok(fisioterapeutaSalvo);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable Long id) {
		try {
			
			Fisioterapeuta fisioterapeutaRecuperado = fisioterapeutaRepository.findById(id).get();
			
			fisioterapeutaRepository.delete(fisioterapeutaRecuperado);
			
			return ResponseEntity.noContent().build();
			
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
			
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(e.getMessage());
		}
	}
	
}
