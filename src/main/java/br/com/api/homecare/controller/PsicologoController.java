package br.com.api.homecare.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Psicologo;
import br.com.api.homecare.model.Psicologo;
import br.com.api.homecare.model.Psicologo;
import br.com.api.homecare.repository.FisioterapeutaRepository;
import br.com.api.homecare.repository.PacienteRepository;
import br.com.api.homecare.repository.PsicologoRepository;
import br.com.api.homecare.service.CadastroFisioterapeutaService;
import br.com.api.homecare.service.CadastroPacienteService;
import br.com.api.homecare.service.CadastroPsicologoService;

@RestController
@RequestMapping(value = "/psicologos")
public class PsicologoController {

	@Autowired
	private PsicologoRepository psicologoRepository;
	
	@Autowired
	private CadastroPsicologoService cadastroPsicologoService;
	
	@GetMapping
	public List<Psicologo> listar() {
		return psicologoRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Psicologo> buscar(@PathVariable Long id) {
		Optional<Psicologo> psicologo = psicologoRepository.findById(id);
		
		if (psicologo.isPresent()) {
			return ResponseEntity.ok(psicologo.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Psicologo adicionar(@RequestBody Psicologo psicologo) {
		return cadastroPsicologoService.salvar(psicologo);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Psicologo> atualizar(@PathVariable Long id,
			@RequestBody Psicologo psicologo) {
		Optional<Psicologo> psicologoAtual = psicologoRepository.findById(id);
		
		if (psicologoAtual.isPresent()) {
			BeanUtils.copyProperties(psicologo, psicologoAtual.get(), "id");
			
			Psicologo psicologoSalvo = cadastroPsicologoService.salvar(psicologoAtual.get());
			return ResponseEntity.ok(psicologoSalvo);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable Long id) {
		try {
			
			Psicologo psicologoRecuperado = psicologoRepository.findById(id).get();
			
			psicologoRepository.delete(psicologoRecuperado);
			
			return ResponseEntity.noContent().build();
			
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
			
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(e.getMessage());
		}
	}
	
}
