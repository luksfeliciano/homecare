package br.com.api.homecare.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Fonoaudiologo;
import br.com.api.homecare.model.Fonoaudiologo;
import br.com.api.homecare.model.Fonoaudiologo;
import br.com.api.homecare.repository.FisioterapeutaRepository;
import br.com.api.homecare.repository.FonoaudiologoRepository;
import br.com.api.homecare.repository.PacienteRepository;
import br.com.api.homecare.repository.PsicologoRepository;
import br.com.api.homecare.service.CadastroFisioterapeutaService;
import br.com.api.homecare.service.CadastroFonoaudiologoService;
import br.com.api.homecare.service.CadastroPacienteService;
import br.com.api.homecare.service.CadastroPsicologoService;

@RestController
@RequestMapping(value = "/fonoaudiologos")
public class FonoaudiologoController {

	@Autowired
	private FonoaudiologoRepository fonoaudiologoRepository;
	
	@Autowired
	private CadastroFonoaudiologoService cadastroFonoaudiologoService;
	
	@GetMapping
	public List<Fonoaudiologo> listar() {
		return fonoaudiologoRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Fonoaudiologo> buscar(@PathVariable Long id) {
		Optional<Fonoaudiologo> fonoaudiologo = fonoaudiologoRepository.findById(id);
		
		if (fonoaudiologo.isPresent()) {
			return ResponseEntity.ok(fonoaudiologo.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Fonoaudiologo adicionar(@RequestBody Fonoaudiologo fonoaudiologo) {
		return cadastroFonoaudiologoService.salvar(fonoaudiologo);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Fonoaudiologo> atualizar(@PathVariable Long id,
			@RequestBody Fonoaudiologo fonoaudiologo) {
		Optional<Fonoaudiologo> fonoaudiologoAtual = fonoaudiologoRepository.findById(id);
		
		if (fonoaudiologoAtual.isPresent()) {
			BeanUtils.copyProperties(fonoaudiologo, fonoaudiologoAtual.get(), "id");
			
			Fonoaudiologo fonoaudiologoSalvo = cadastroFonoaudiologoService.salvar(fonoaudiologoAtual.get());
			return ResponseEntity.ok(fonoaudiologoSalvo);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable Long idFono) {
		try {
			
			Fonoaudiologo fonoaudiologoRecuperado = fonoaudiologoRepository.findById(idFono).get();
			
			fonoaudiologoRepository.delete(fonoaudiologoRecuperado);
			
			return ResponseEntity.noContent().build();
			
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
			
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(e.getMessage());
		}
	}
	
}
