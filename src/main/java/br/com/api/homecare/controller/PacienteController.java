package br.com.api.homecare.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.homecare.exception.EntidadeEmUsoException;
import br.com.api.homecare.exception.EntidadeNaoEncontradaException;
import br.com.api.homecare.model.Paciente;
import br.com.api.homecare.repository.PacienteRepository;
import br.com.api.homecare.service.CadastroPacienteService;

@RestController
@RequestMapping(value = "/pacientes")
public class PacienteController {

	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Autowired
	private CadastroPacienteService cadastroPacienteService;
	
	@GetMapping
	public List<Paciente> listar() {
		return pacienteRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Paciente> buscar(@PathVariable Long id) {
		Optional<Paciente> paciente = pacienteRepository.findById(id);
		
		if (paciente.isPresent()) {
			return ResponseEntity.ok(paciente.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Paciente adicionar(@RequestBody Paciente paciente) {
		return cadastroPacienteService.salvar(paciente);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Paciente> atualizar(@PathVariable Long id,
			@RequestBody Paciente paciente) {
		Optional<Paciente> pacienteAtual = pacienteRepository.findById(id);
		
		if (pacienteAtual.isPresent()) {
			BeanUtils.copyProperties(paciente, pacienteAtual.get(), "id");
			
			Paciente pacienteSalvo = cadastroPacienteService.salvar(pacienteAtual.get());
			return ResponseEntity.ok(pacienteSalvo);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable Long id) {
		try {
			
			Paciente pacienteRecuperado = pacienteRepository.findById(id).get();
			
			pacienteRecuperado.setStPaciente(false);
			
			pacienteRepository.save(pacienteRecuperado);
			
			return ResponseEntity.noContent().build();
			
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
			
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(e.getMessage());
		}
	}
	
}
